import React, {createContext, useState} from 'react';
import {getProductList} from '../../api';
import {showError} from '../../utils';

export const AttedanceContext = createContext();

export function AttendanceProvider({children}) {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const getListProducts = async () => {
    const events = await getProductList();
    setLoading(loading);
    if (events.error) {
      setLoading(false);
      showError(events.message ?? 'Failed get product list');
    }
    if (events.success) {
      setLoading(false);
      setProducts([...products, ...events.data]);
    }
  };

  return (
    <AttedanceContext.Provider
      value={{
        products,
        loading,
        getListProducts,
      }}>
      {children}
    </AttedanceContext.Provider>
  );
}
