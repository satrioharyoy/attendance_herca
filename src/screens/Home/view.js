import React from 'react';
import {View} from 'react-native';
import {Footer, Header, ListContact, Loading} from '../../components';
import {styles} from './styles';

const ListUsersComponent = ({listProduct, loading}) => {
  return (
    <>
      <View style={styles.container}>
        <Header
          icon={'iconMobile'}
          headerTitle={'Product List'}
          type={'dark'}
        />
        {listProduct.length > 0 && !loading && (
          <View>
            {listProduct?.length > 0 && (
              <ListContact
                dataListContacts={listProduct}
                // onPressDetail={id => onPressDetail(id)}
                // onPressDeleteContact={id => onPressDeleteContact(id)}
                totalContacts={listProduct.length}
              />
            )}
            <Footer />
          </View>
        )}
        {loading && (
          <View
            style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Loading />
          </View>
        )}
      </View>
    </>
  );
};

export default ListUsersComponent;
