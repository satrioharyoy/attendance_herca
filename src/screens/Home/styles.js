import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../utils';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
