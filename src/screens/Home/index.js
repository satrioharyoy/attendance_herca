import React, {useEffect, useState} from 'react';
import {getProductList} from '../../api';
import HomeComponent from './view';
import {showError} from '../../utils';

const Home = ({navigation}) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const getListProducts = async () => {
    setLoading(true);
    const events = await getProductList();
    if (events.error) {
      setLoading(false);
      showError(events.message ?? 'Failed get product list');
    }
    if (events.success) {
      setLoading(false);
      setProducts([...products, ...events.data]);
    }
  };

  useEffect(() => {
    getListProducts();
  }, []);

  console.log('check loading => ', loading);

  return <HomeComponent listProduct={products} loading={loading} />;
};

export default Home;
