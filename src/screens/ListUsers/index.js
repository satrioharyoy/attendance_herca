import React, {useContext, useState} from 'react';
import {AttedanceContext} from '../../context/userContext';
import ListUsersComponent from './view';

const ListUser = ({navigation}) => {
  const {userData, deleteUser} = useContext(AttedanceContext);
  const [onDeleteModal, setOnDeleteModal] = useState(false);
  const [selectedId, setSelectedId] = useState(null);

  const onPressDetailUser = id => {
    navigation.navigate('UserProfile', {id: id, fromEdit: false});
  };

  const onDeleteContact = () => {
    setOnDeleteModal(false);
    deleteUser(selectedId);
  };

  const onPressDeleteButton = id => {
    setSelectedId(id);
    setOnDeleteModal(true);
  };

  return (
    <ListUsersComponent
      onPressBack={() => navigation.goBack()}
      listContacts={userData}
      onPressDetail={id => onPressDetailUser(id)}
      onPressDeleteContact={id => onPressDeleteButton(id)}
      totalContacts={userData.length}
      onPressAddContact={() => navigation.navigate('AddContact')}
      onDeleteModal={onDeleteModal}
      onCancelModalButton={() => setOnDeleteModal(false)}
      onPressDeleteButton={onDeleteContact}
    />
  );
};

export default ListUser;
