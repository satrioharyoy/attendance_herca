import React from 'react';
import {View} from 'react-native';
import {Button, Gap, Header, Profile, ProfileItem} from '../../components';
import {styles} from './styles';

const UserProfileComponent = ({
  detailUser,
  onPressBack,
  onPressEditProfile,
}) => {
  return (
    <>
      <View style={styles.container}>
        <Header headerTitle="Profile" onPress={onPressBack} />
        <Gap height={24} />
        <Profile
          name={detailUser?.name}
          desc={detailUser?.phone}
          avatar={detailUser?.image ? {uri: detailUser?.image} : null}
          icon={detailUser?.gender}
        />
        <Gap height={10} />
        <ProfileItem label="Email" value={detailUser?.email} />
        <View style={styles.buttonWrapper}>
          <Button title="Edit Profile" onPress={onPressEditProfile} />
        </View>
      </View>
    </>
  );
};

export default UserProfileComponent;
