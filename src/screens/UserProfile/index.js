import React, {useContext, useEffect, useState} from 'react';
import {AttedanceContext} from '../../context/userContext';
import UserProfileComponent from './view';

const UserProfile = ({navigation, route}) => {
  const idUser = route.params.id;
  const fromEdit = route?.params?.fromEdit ?? null;
  const {getDetailUser} = useContext(AttedanceContext);
  const [userDetail, setUserDetail] = useState(null);

  useEffect(() => {
    if (!fromEdit) {
      const dataUserDetail = getDetailUser(idUser);
      setUserDetail(dataUserDetail);
    }
  }, [fromEdit, getDetailUser, idUser]);

  return (
    <UserProfileComponent
      onPressBack={() => navigation.goBack()}
      detailUser={userDetail}
      onPressEditProfile={() =>
        navigation.replace('AddContact', {userId: userDetail.id})
      }
    />
  );
};

export default UserProfile;
