import React from 'react';
import {ScrollView, View} from 'react-native';
import {Button, Gap, Header, InputText} from '../../components';
import {styles} from './styles';

const AddContactComponent = ({onPressgoBack, form, doOnSubmit, setForm}) => {
  return (
    <>
      <View style={styles.container}>
        <Header
          headerTitle="Daftar Akun"
          icon="iconBackDark"
          onPress={onPressgoBack}
        />
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <InputText
              label="Name"
              value={form.name}
              onChangeText={value => setForm('name', value)}
            />
            <Gap height={24} />
            <InputText
              label="Phone"
              value={form.phone}
              onChangeText={value => setForm('phone', value)}
            />
            <Gap height={24} />
            <InputText
              label="Email"
              value={form.email}
              onChangeText={value => setForm('email', value)}
              inputMode="email"
              keyboardType="email-address"
            />
            <Gap height={24} />
            <Button title="Continue" onPress={() => doOnSubmit()} />
          </ScrollView>
        </View>
      </View>
    </>
  );
};

export default AddContactComponent;
