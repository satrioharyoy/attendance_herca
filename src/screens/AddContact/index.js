import React, {useContext, useEffect} from 'react';
import uuid from 'react-native-uuid';
import {AttedanceContext} from '../../context/userContext';
import {useForm} from '../../utils';
import AddContactComponent from './view';

const SignUp = ({navigation, route}) => {
  const userId = route?.params?.userId ?? null;
  const [form, setForm] = useForm({
    name: '',
    avatar: null,
    phone: '',
    email: '',
  });
  const {addNewUser, getDetailUser, editDataUser} =
    useContext(AttedanceContext);

  useEffect(() => {
    if (userId) {
      const userDetailData = getDetailUser(userId);
      setForm('newValue', {...userDetailData});
    }
  }, [getDetailUser, userId]);

  const doOnSubmit = async () => {
    if (userId) {
      editDataUser(form, navigation.replace('MainApp', {screen: 'Users'}));
    } else {
      addNewUser(
        {...form, id: uuid.v4()},
        navigation.replace('MainApp', {screen: 'Users'}),
      );
    }
  };

  return (
    <AddContactComponent
      doOnSubmit={doOnSubmit}
      form={form}
      onPressgoBack={() => navigation.goBack()}
      setForm={setForm}
    />
  );
};

export default SignUp;
