import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome6';
import {ListContact} from '../../components';
import {DeleteWarningModal, Header, Loading} from '../../components/molecules';
import {styles} from './styles';

const ListAttendanceComponent = ({
  listContacts,
  onPressDetail,
  onPressDeleteContact,
  totalContacts,
  onPressAddAttendanceData,
  onDeleteModal,
  onCancelModalButton,
  onPressDeleteButton,
}) => {
  return (
    <>
      <View style={styles.container}>
        <Header headerTitle={'User Attendance'} type="dark" />
        <View style={styles.contentSection}>
          {listContacts?.length > 0 && (
            <ListContact
              dataListContacts={listContacts}
              onPressDetail={id => onPressDetail(id)}
              onPressDeleteContact={onPressDeleteContact}
              totalContacts={totalContacts}
            />
          )}
        </View>
        <TouchableOpacity
          onPress={onPressAddAttendanceData}
          style={styles.floatButton}>
          <Icon name="plus" size={30} color={'white'} solid />
        </TouchableOpacity>
      </View>
      {listContacts && listContacts.length < 1 && <Loading />}
      {onDeleteModal && (
        <DeleteWarningModal
          visible={onDeleteModal}
          onCancel={onCancelModalButton}
          onConfirm={onPressDeleteButton}
        />
      )}
    </>
  );
};

export default ListAttendanceComponent;
