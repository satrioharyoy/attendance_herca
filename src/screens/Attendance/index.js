import React, {useContext, useEffect, useState} from 'react';
import {AttedanceContext} from '../../context/userContext';
import ListAttendanceComponent from './view';

const Attedance = ({navigation}) => {
  const {getAttendanceUser, deleDataAttendance} = useContext(AttedanceContext);
  const [dataAttendance, setDataAttendance] = useState([]);
  const [onModalDeleteAttendance, setOnDeleteModalAttendance] = useState(false);
  const [selectedId, setSelectedId] = useState(null);

  const onPressDetailUser = id => {
    navigation.navigate('AttendanceDetail', {
      idAttendance: id,
      fromEdit: false,
    });
  };

  const onDeleteContact = () => {
    setOnDeleteModalAttendance(false);
    deleDataAttendance(selectedId);
  };

  useEffect(() => {
    const getUserAttendance = getAttendanceUser();
    setDataAttendance(getUserAttendance);
  }, [getAttendanceUser]);

  const onPressDeleteButton = id => {
    setSelectedId(id);
    setOnDeleteModalAttendance(true);
  };

  return (
    <ListAttendanceComponent
      onPressBack={() => navigation.goBack()}
      listContacts={dataAttendance}
      onPressDetail={id => onPressDetailUser(id)}
      onPressDeleteContact={onPressDeleteButton}
      totalContacts={dataAttendance.length}
      onPressAddAttendanceData={() => navigation.navigate('EditAttendance')}
      onDeleteModal={onModalDeleteAttendance}
      onCancelModalButton={() => setOnDeleteModalAttendance(false)}
      onPressDeleteButton={onDeleteContact}
    />
  );
};

export default Attedance;
