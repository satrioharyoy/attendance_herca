import React from 'react';
import {DummyDataMyBooking} from '../../assets';
import ListUsersComponent from './view';

const ListUser = ({navigation}) => {
  const onPressDetailUser = id => {
    navigation.navigate('AboutHomestay');
  };

  return (
    <ListUsersComponent
      onPressBack={() => navigation.goBack()}
      listMyBookings={DummyDataMyBooking.vacationBookings}
      onPressDetail={id => onPressDetailUser(id)}
    />
  );
};

export default ListUser;
