import React from 'react';
import {View} from 'react-native';
import {ListMyBookings} from '../../components';
import {Header} from '../../components/molecules';
import {styles} from './styles';

const ListUsersComponent = ({listMyBookings, onPressDetail}) => {
  return (
    <>
      <View style={styles.container}>
        <Header
          headerTitle={'My Bookings'}
          icon={'iconBackDark'}
          iconRight={'iconHamburger'}
        />
        <View>
          {listMyBookings?.length > 0 && (
            <ListMyBookings
              dataListMybooking={listMyBookings}
              onPressDetail={id => onPressDetail(id)}
            />
          )}
        </View>
      </View>
    </>
  );
};

export default ListUsersComponent;
