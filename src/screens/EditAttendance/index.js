import React, {useContext, useEffect, useState} from 'react';
import moment from 'moment';
import uuid from 'react-native-uuid';
import {AttedanceContext} from '../../context/userContext';
import {useForm} from '../../utils';
import EditAttendanceComponent from './view';

const EditAttendance = ({navigation, route}) => {
  const userId = route?.params?.userId ?? null;
  const [form, setForm] = useForm({
    id: '',
    user_id: '',
    name: '',
    date: '',
    clock_in: '',
    clock_out: '',
  });
  const {
    getAttendanceUserDetail,
    editAttendanceData,
    getUserOptionList,
    addNewAttendance,
  } = useContext(AttedanceContext);
  const [openDate, setOpenDate] = useState({open: false, datePickerFor: null});
  const [date, setDate] = useState(null);
  const [onClockin, setOnClockin] = useState(null);
  const [onClockout, sesetOnClockout] = useState(null);
  const [mode, setMode] = useState('date');
  const [userOptionList, setUserOptionList] = useState([]);
  const [userSelected, setUserSelected] = useState(null);

  useEffect(() => {
    if (userId) {
      const getAttendanceDetail = getAttendanceUserDetail(userId);
      setForm('newValue', {...getAttendanceDetail});
    } else {
      const userList = getUserOptionList();
      setUserOptionList(userList);
    }
  }, [getAttendanceUserDetail, userId]);

  const doOnSubmit = async () => {
    if (userId) {
      editAttendanceData(
        {
          id: form.id,
          user_id: form.user_id,
          date: moment(date).format('YYYY-MM-DD'),
          clock_in: moment(onClockin).format('HH:mm'),
          clock_out: moment(onClockout).format('HH:mm'),
        },
        navigation.replace('MainApp', {screen: 'Attendance'}),
      );
    } else {
      addNewAttendance(
        {
          id: uuid.v4(),
          user_id: userSelected,
          date: moment(date).format('YYYY-MM-DD'),
          clock_in: moment(onClockin).format('HH:mm'),
          clock_out: moment(onClockout).format('HH:mm'),
        },
        navigation.replace('MainApp', {screen: 'Attendance'}),
      );
    }
  };

  const openDatePicker = (modeDatePicker, datePickerFor) => {
    setMode(modeDatePicker);
    setOpenDate({open: true, datePickerFor: datePickerFor});
  };

  const onConfirmDate = date => {
    setOpenDate({open: false, datePickerFor: null});
    switch (openDate.datePickerFor) {
      case 'date':
        setDate(date);
        break;
      case 'clockIn':
        setOnClockin(date);
        break;
      case 'clockOut':
        sesetOnClockout(date);
        break;
      default:
        break;
    }
  };

  return (
    <EditAttendanceComponent
      pressGoback={() => navigation.goBack()}
      date={date}
      doOnSubmit={doOnSubmit}
      form={form}
      mode={mode}
      onClockin={onClockin}
      onClockout={onClockout}
      onConfirmDate={onConfirmDate}
      openDatePicker={openDatePicker}
      openDate={openDate}
      setForm={setForm}
      setOpenDate={setOpenDate}
      optionList={userOptionList}
      userSelected={userSelected}
      setUserSelected={id => setUserSelected(id)}
    />
  );
};

export default EditAttendance;
