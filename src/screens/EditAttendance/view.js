import moment from 'moment';
import React from 'react';
import {Pressable, ScrollView, View} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {Button, Gap, Header, InputText, PickerSelect} from '../../components';
import {styles} from './styles';

const EditAttendanceComponent = ({
  pressGoback,
  form,
  setForm,
  openDatePicker,
  date,
  onClockin,
  onClockout,
  doOnSubmit,
  openDate,
  mode,
  onConfirmDate,
  setOpenDate,
  optionList,
  userSelected,
  setUserSelected,
}) => {
  return (
    <>
      <View style={styles.container}>
        <Header
          headerTitle="Pembaruan Data Kehadiran"
          icon="iconBackDark"
          onPress={pressGoback}
        />
        <View style={styles.content}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <PickerSelect
              label="Name"
              optionList={optionList}
              value={userSelected}
              setValue={value => setUserSelected(value)}
              placeholder={form?.name ? form?.name : 'Select User...'}
              disabled={form?.user_id ? true : false}
            />
            <Gap height={24} />
            <Pressable onPress={() => openDatePicker('date', 'date')}>
              <InputText
                label="Tanggal Masuk"
                value={date ? moment(date).format('YYYY-MM-DD') : form.date}
                onChangeText={value => setForm('date', value)}
                disable
              />
            </Pressable>
            <Gap height={24} />
            <Pressable onPress={() => openDatePicker('time', 'clockIn')}>
              <InputText
                label="Jam Masuk"
                value={
                  onClockin ? moment(onClockin).format('HH:mm') : form.clock_in
                }
                onChangeText={value => setForm('clock_in', value)}
                disable
              />
            </Pressable>
            <Gap height={24} />
            <Pressable onPress={() => openDatePicker('time', 'clockOut')}>
              <InputText
                label="Jam Keluar"
                value={
                  onClockout
                    ? moment(onClockout).format('HH:mm')
                    : form.clock_out
                }
                onChangeText={value => setForm('clock_out', value)}
                disable
              />
            </Pressable>
            <Gap height={24} />
            <Button title="Continue" onPress={() => doOnSubmit()} />
          </ScrollView>
        </View>
      </View>
      {openDate && (
        <DatePicker
          modal
          open={openDate.open}
          mode={mode}
          date={date ?? new Date()}
          onConfirm={date => onConfirmDate(date)}
          onCancel={() => {
            setOpenDate(false);
          }}
          is24hourSource="locale"
        />
      )}
    </>
  );
};

export default EditAttendanceComponent;
