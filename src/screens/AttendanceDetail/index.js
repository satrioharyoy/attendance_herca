import React, {useContext, useEffect, useState} from 'react';
import {AttedanceContext} from '../../context/userContext';
import AttendanceDetailComponent from './view';

const AttedanceDetail = ({navigation, route}) => {
  const idAttendance = route.params.idAttendance;
  const [attendanceUserDetail, setAttedanceUserDetail] = useState(null);
  const {getAttendanceUserDetail} = useContext(AttedanceContext);

  useEffect(() => {
    const dataAttendanceUserDetail = getAttendanceUserDetail(idAttendance);
    setAttedanceUserDetail(dataAttendanceUserDetail);
  }, [getAttendanceUserDetail, idAttendance]);

  return (
    <AttendanceDetailComponent
      onPressBack={() => navigation.goBack()}
      detailUser={attendanceUserDetail}
      onPressEditProfile={() =>
        navigation.navigate('EditAttendance', {
          userId: attendanceUserDetail.user_id,
        })
      }
    />
  );
};

export default AttedanceDetail;
