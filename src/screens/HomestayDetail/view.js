import React from 'react';
import {Image, ScrollView, Text, View} from 'react-native';
import {
  BeachIcon,
  BreakfastIcon,
  FitnessIcon,
  ImageNotFoundILL,
  WifiIcon,
} from '../../assets';
import {
  Button,
  DescriptionText,
  Gap,
  LocationName,
  RatingText,
} from '../../components';
import {Header} from '../../components/molecules';
import {styles} from './styles';

const ListUsersComponent = ({onPressBack}) => {
  return (
    <>
      <View style={styles.container}>
        <Header
          headerTitle={'About Homestay'}
          icon={'iconBackDark'}
          iconRight={'iconWhislist'}
          onPress={onPressBack}
        />
        <ScrollView
          style={styles.contentSection}
          showsVerticalScrollIndicator={false}>
          <View style={styles.dummyImageContainer}>
            <Image source={ImageNotFoundILL} style={styles.dummyImage} />
          </View>
          <View style={styles.homestayPriceContainer}>
            <View>
              <Text style={styles.homestayNameText}>Royal Oasis Resort</Text>
              <Gap height={7} />
              <LocationName location={'Anyer'} province={'Serang'} />
              <Gap height={7} />
              <RatingText rating={'4.9'} />
            </View>
            <View style={styles.priceTextContainer}>
              <Text style={styles.priceText}>$3,899</Text>
              <Text style={styles.pernightText}>Per night</Text>
            </View>
          </View>
          <View style={styles.homestayPriceContainer}>
            <View>
              <Text style={styles.homestayNameText}>Facility</Text>
            </View>
            <View>
              <Text style={styles.seeAllText}>See All</Text>
            </View>
          </View>
          <View style={styles.facilityIconContainer}>
            <View style={styles.facilityIconSection}>
              <WifiIcon />
              <Text style={styles.descIconText}>Free wifi</Text>
            </View>
            <View style={styles.facilityIconSection}>
              <FitnessIcon />
              <Text style={styles.descIconText}>Fitness</Text>
            </View>
            <View style={styles.facilityIconSection}>
              <BreakfastIcon />
              <Text style={styles.descIconText}>Breakfast</Text>
            </View>
            <View style={styles.facilityIconSection}>
              <BeachIcon />
              <Text style={styles.descIconText}>Beach</Text>
            </View>
          </View>
          <Gap height={20} />
          <Text style={styles.homestayNameText}>Description</Text>
          <Gap height={10} />
          <DescriptionText
            text={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas lobortis orci eu tincidunt tempus. Aliquam pulvinar ante a odio aliquam varius. Phasellus ipsum quam, auctor eget magna nec, lobortis vehicula arcu. Integer maximus vehicula libero eget gravida. Curabitur tristique venenatis ligula, vitae vulputate lectus sodales vel. Vivamus at varius magna, ac semper tellus. Quisque rhoncus nibh varius tellus placerat, non sagittis est gravida. Nullam dapibus pellentesque nisl id accumsan. Morbi euismod sapien id condimentum euismod. Suspendisse vel efficitur risus. Quisque auctor felis eget velit vehicula, vel bibendum odio cursus.
                   Phasellus aliquam imperdiet odio. Ut porttitor ante eu auctor venenatis. Aenean rutrum consectetur metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean tellus massa, interdum sed elit nec, dictum elementum est. Pellentesque in elit facilisis, bibendum dui a, bibendum odio. Sed fermentum eros sed congue egestas. Ut pretium eleifend iaculis.`}
          />
        </ScrollView>
        <View style={styles.buttonContainer}>
          <Button title={'Book Now'} styleButton={styles.button} />
        </View>
      </View>
    </>
  );
};

export default ListUsersComponent;
