import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../utils';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 25,
  },
  contentSection: {
    flex: 1,
  },
  floatButton: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 60,
    backgroundColor: colors.primary,
    borderRadius: 100,
  },
  buttonContainer: {
    paddingVertical: 20,
  },
  button: {borderRadius: 20, paddingVertical: 15},
  dummyImageContainer: {
    height: 218,
    borderRadius: 20,
    backgroundColor: colors.borders,
    justifyContent: 'center',
    alignItems: 'center',
  },
  homestayPriceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  homestayNameText: {
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    fontSize: 18,
  },
  priceTextContainer: {marginTop: 10},
  priceText: {
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    fontSize: 24,
  },
  pernightText: {fontSize: 14, textAlign: 'right'},
  seeAllText: {
    fontFamily: fonts.primary[500],
    fontSize: 14,
    color: colors.text.secondary,
  },
  facilityIconContainer: {
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  facilityIconSection: {alignItems: 'center'},
  descIconText: {
    fontFamily: fonts.primary[400],
    textAlign: 'center',
    marginTop: 5,
  },
});
