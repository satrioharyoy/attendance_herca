import React from 'react';
import ListUsersComponent from './view';

const ListUser = ({navigation}) => {
  return <ListUsersComponent onPressBack={() => navigation.goBack()} />;
};

export default ListUser;
