import {View, Text, FlatList, ActivityIndicator} from 'react-native';
import React, {useCallback} from 'react';
import {styles} from './styles';
import {ListItem} from '../../molecules';
import {colors} from '../../../utils';

const ListContacts = ({
  dataListContacts,
  onPressDetail,
  isLoading,
  onPressDeleteContact,
}) => {
  const renderItem = useCallback(item => {
    return (
      <ListItem
        key={`${item?.item?.id}`}
        name={`${item?.item?.title}`}
        price={`${item?.item?.price}`}
        type="next"
        // onPressEdit={() => onPressDetail(item?.item?.id)}
        // onPressDeleteContact={() => onPressDeleteContact(item?.item?.id)}
      />
    );
  }, []);

  const keyExtractorItem = useCallback(
    (item, index) => `${item.id}_${index}`,
    [],
  );

  const activityIndicator = useCallback(() => {
    if (isLoading) {
      return <ActivityIndicator color={colors.primary} />;
    } else {
      return null;
    }
  }, [isLoading]);

  const EmptyListComponent = useCallback(() => {
    if (!isLoading) {
      return (
        <View style={styles.emptyContainer}>
          <Text bold align="center">
            {"Contact Can't found"}
          </Text>
        </View>
      );
    }
  }, [isLoading]);

  return (
    <FlatList
      data={dataListContacts}
      renderItem={renderItem}
      keyExtractor={keyExtractorItem}
      ListEmptyComponent={EmptyListComponent}
      showsVerticalScrollIndicator={false}
      extraData={dataListContacts}
      initialNumToRender={10}
      maxToRenderPerBatch={10}
      ListFooterComponent={activityIndicator}
      style={{marginBottom: 90}}
    />
  );
};

export default ListContacts;
