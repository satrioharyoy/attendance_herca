import React, {useCallback} from 'react';
import {ActivityIndicator, FlatList, Text, View} from 'react-native';
import {colors} from '../../../utils';
import {ListMybookingItem} from '../../molecules';
import {styles} from './styles';
import moment from 'moment';

const ListMybookings = ({dataListMybooking, onPressDetail, isLoading}) => {
  const renderItem = useCallback(
    item => {
      return (
        <ListMybookingItem
          key={`${item?.item?.id}`}
          checkInDate={moment(item?.item?.checkInDate).format('DD MMM')}
          bookingDays={moment(item?.item?.checkOutDate).diff(
            item?.item?.checkInDate,
            'days',
          )}
          location={`${item?.item?.location}`}
          vacationSpot={`${item?.item?.vacationSpot}`}
          province={`${item?.item?.province}`}
          totalPrice={`${item?.item?.totalPrice}`}
          type="next"
          onPressDetail={() => onPressDetail(item?.item?.id)}
        />
      );
    },
    [onPressDetail],
  );

  const keyExtractorItem = useCallback(
    (item, index) => `${item.id}_${index}`,
    [],
  );

  const activityIndicator = useCallback(() => {
    if (isLoading) {
      return <ActivityIndicator color={colors.primary} />;
    } else {
      return null;
    }
  }, [isLoading]);

  const EmptyListComponent = useCallback(() => {
    if (!isLoading) {
      return (
        <View style={styles.emptyContainer}>
          <Text bold align="center">
            {"Contact Can't found"}
          </Text>
        </View>
      );
    }
  }, [isLoading]);

  return (
    <FlatList
      data={dataListMybooking}
      renderItem={renderItem}
      keyExtractor={keyExtractorItem}
      ListEmptyComponent={EmptyListComponent}
      showsVerticalScrollIndicator={false}
      extraData={dataListMybooking}
      initialNumToRender={10}
      maxToRenderPerBatch={10}
      ListFooterComponent={activityIndicator}
      style={{marginBottom: 90}}
    />
  );
};

export default ListMybookings;
