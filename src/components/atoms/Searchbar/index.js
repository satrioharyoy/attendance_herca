import React from 'react';
import {StyleSheet, TextInput, TouchableOpacity, View} from 'react-native';
import {FilterIcon, SearchIcon} from '../../../assets';
import {colors} from '../../../utils';

const SearchBar = ({onSearch, onFilter, placeholder}) => {
  return (
    <View style={styles.container}>
      <SearchIcon />
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        onChangeText={onSearch}
      />
      <TouchableOpacity onPress={onFilter}>
        <FilterIcon />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    borderRadius: 30,
    borderColor: colors.borders,
    borderWidth: 1,
  },
  input: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
  },
});

export default SearchBar;
