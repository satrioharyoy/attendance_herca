import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {colors, fonts} from '../../../utils';

const PickerSelectComponent = ({
  label,
  optionList,
  value,
  setValue,
  placeholder,
  disabled,
}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <RNPickerSelect
        onValueChange={value => setValue(value)}
        items={optionList}
        style={{
          ...pickerSelectStyles,
          iconContainer: {
            top: 10,
            right: 12,
          },
        }}
        useNativeAndroidPickerStyle={false}
        placeholder={{
          label: placeholder,
          value: null,
        }}
        value={value}
        textInputProps={{underlineColor: 'yellow'}}
        disabled={disabled}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    marginBottom: 6,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: colors.borders,
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: colors.borders,
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

export default PickerSelectComponent;
