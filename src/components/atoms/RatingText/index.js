import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {StarIcon} from '../../../assets';
import {colors, fonts} from '../../../utils';

const RatingText = ({rating, size = 'medium', type = 'dark'}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.textRating(size, type)}>{rating}</Text>
      <StarIcon height={size === 'small' ? 18 : 24} />
    </View>
  );
};

export default RatingText;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textRating: (size, type) => ({
    fontFamily: fonts.primary[400],
    fontSize: size === 'small' ? 12 : 16,
    color: type === 'light' ? colors.white : colors.text.primary,
    marginRight: 5,
  }),
});
