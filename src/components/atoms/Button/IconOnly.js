import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome6';
import {MobileIcon} from '../../../assets';

const IconOnly = ({icon}) => {
  switch (icon) {
    case 'iconMobile':
      return <MobileIcon />;
    default:
      return <Icon name="arrow-left" solid size={20} color={'black'} />;
  }
};

export default IconOnly;
