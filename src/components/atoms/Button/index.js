import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, fonts} from '../../../utils';
import IconOnly from './IconOnly';

const Button = ({
  type,
  title,
  onPress,
  icon,
  disable,
  styleButton,
  disableTextStyle,
}) => {
  if (disable) {
    return (
      <View style={[styleButton, styles.disableBG]}>
        <Text style={[styles.disableText, disableTextStyle]}>{title}</Text>
      </View>
    );
  }
  if (type === 'iconOnly') {
    return (
      <TouchableOpacity onPress={onPress}>
        <IconOnly icon={icon} />
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity
      style={[styles.buttonContainer(type), styleButton]}
      onPress={onPress}>
      <Text style={styles.buttonLabel(type)}>{title}</Text>
    </TouchableOpacity>
  );
};
export default Button;
const styles = StyleSheet.create({
  buttonContainer: type => ({
    paddingVertical: 10,
    backgroundColor:
      type === 'secondary'
        ? colors.button.secondary.background
        : colors.button.primary.background,
    borderRadius: 10,
  }),
  disableBG: {
    paddingVertical: 10,
    backgroundColor: colors.buttonDisable.background,
    borderRadius: 10,
  },
  disableText: {
    fontSize: 18,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
    color: colors.buttonDisable.text,
  },
  buttonLabel: type => ({
    fontSize: 18,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
    color:
      type === 'secondary'
        ? colors.button.secondary.text
        : colors.button.primary.text,
  }),
});
