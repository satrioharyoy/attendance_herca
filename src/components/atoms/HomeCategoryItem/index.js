import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {fonts} from '../../../utils';

const HomeCategory = ({category, onPress, isSelected}) => {
  return (
    <TouchableOpacity style={styles.container(isSelected)} onPress={onPress}>
      <Text style={styles.categoryText(isSelected)}>{category}</Text>
    </TouchableOpacity>
  );
};

export default HomeCategory;

const styles = StyleSheet.create({
  container: isSelected => ({
    paddingHorizontal: 12,
    alignSelf: 'flex-start',
    marginRight: 10,
    borderBottomColor: isSelected ? 'rgba(23, 22, 33, 1)' : null,
    borderBottomWidth: isSelected ? 2 : null,
    paddingVertical: 5,
  }),
  categoryText: isSelected => ({
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: isSelected ? 'rgba(23, 22, 33, 1)' : null,
  }),
});
