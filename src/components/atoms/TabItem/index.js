import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  CalendarActivedIcon,
  CalendarInactivedIcon,
  DiscountInactivedIcon,
  HomeActivedIcon,
  HomeInactivedIcon,
  ProfileInactivedIcon,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const TabItem = ({title, active, onPress, onLongPress}) => {
  const GetIcon = () => {
    switch (title) {
      case 'Home':
        return active ? <HomeActivedIcon /> : <HomeInactivedIcon />;
      case 'MyBookings':
        return active ? <CalendarActivedIcon /> : <CalendarInactivedIcon />;
      case 'Promo':
        return <DiscountInactivedIcon />;
      case 'Profile':
        return <ProfileInactivedIcon />;
      default:
        break;
    }
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <GetIcon />
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  text: active => ({
    fontSize: 10,
    color: active ? colors.text.menuActive : colors.text.menuInactive,
    fontFamily: fonts.primary[600],
    marginTop: 4,
  }),
});
