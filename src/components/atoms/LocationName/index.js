import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {PinLocationIcon, PinLocationWhiteIcon} from '../../../assets';
import Gap from '../Gap';
import {colors, fonts} from '../../../utils';

const LocationName = ({location, province, type = 'dark', size}) => {
  return (
    <View style={styles.locationDateSection}>
      {type === 'light' ? <PinLocationWhiteIcon /> : <PinLocationIcon />}
      <Gap width={5} />
      <Text style={styles.textLocation(type, size)}>
        {location}, {province}
      </Text>
    </View>
  );
};

export default LocationName;

const styles = StyleSheet.create({
  locationDateSection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textLocation: (type, size) => ({
    fontSize: size ?? 14,
    fontFamily: fonts.primary[400],
    color: type === 'dark' ? colors.text.primary : 'white',
  }),
});
