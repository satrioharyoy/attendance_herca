import React, {useState} from 'react';
import {Text, TouchableOpacity, StyleSheet, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const ReadMoreText = ({text, numberOfLines = 5}) => {
  const [isReadMore, setIsReadMore] = useState(true);

  const onToggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  return (
    <View style={styles.container}>
      <Text
        numberOfLines={isReadMore ? numberOfLines : null}
        style={styles.text}>
        {isReadMore ? text.substring(0, 120) + '...' : text}
      </Text>
      <TouchableOpacity onPress={onToggleReadMore}>
        <Text style={styles.readMore}>
          {isReadMore ? 'Read More' : 'Read Less'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  text: {
    fontSize: 14,
    marginRight: 5,
    fontFamily: fonts.primary[400],
  },
  readMore: {
    fontSize: 14,
    color: colors.text.secondary,
    fontFamily: fonts.primary[500],
  },
});

export default ReadMoreText;
