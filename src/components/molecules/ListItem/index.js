import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';

const ListProductItem = ({name, price}) => {
  const [total, setTotal] = useState(0);
  return (
    <View style={styles.container}>
      <View>
        <Text numberOfLines={2} style={styles.nameText}>
          {name}
        </Text>
        <Text style={styles.priceText}>{price}</Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title={'-'}
          styleButton={styles.button}
          disable={total === 0}
          disableTextStyle={styles.buttonText}
        />
        {/* <Text style={styles.buttonText}>+</Text> */}
        <Gap width={30} />
        <Text style={styles.totalText}>{total}</Text>
        <Gap width={30} />
        <Button
          title={'+'}
          styleButton={styles.button}
          // disable={total === 0}
          disableTextStyle={styles.buttonText}
        />
      </View>
    </View>
  );
};

export default ListProductItem;

const styles = StyleSheet.create({
  container: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameText: {
    fontSize: 16,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
  },
  priceText: {
    fontSize: 14,
    fontFamily: fonts.primary[700],
  },
  totalText: {
    fontSize: 20,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    borderWidth: 1,
    borderColor: '#F0F0F0',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
  },
  buttonText: {
    fontSize: 20,
    fontFamily: fonts.primary[700],
    color: colors.white,
  },
  button: {
    paddingVertical: 8,
    backgroundColor: '#3A4144',
    paddingHorizontal: 20,
  },
});
