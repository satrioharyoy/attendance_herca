import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ClockIcon, DateIcon, ImageNotFoundILL} from '../../../assets';
import {colors, fonts} from '../../../utils';
import {Gap, LocationName} from '../../atoms';

const ListMybookingItem = ({
  vacationSpot,
  location,
  province,
  checkInDate,
  totalPrice,
  bookingDays,
  onPressDetail,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.dummyImageContainer}>
        <Image source={ImageNotFoundILL} style={styles.dummyImage} />
      </View>
      <TouchableOpacity onPress={onPressDetail} style={styles.itemSection}>
        <Text style={styles.title}>{vacationSpot}</Text>

        <Gap height={8} />
        <LocationName size={12} location={location} province={province} />
        <Gap height={8} />
        <View style={styles.locationDateSection}>
          <View style={styles.locationDateSection}>
            <DateIcon />
            <Gap width={5} />
            <Text style={styles.dateText}>{checkInDate}</Text>
          </View>
          <Gap width={12} />
          <View style={styles.locationDateSection}>
            <ClockIcon />
            <Gap width={5} />
            <Text style={styles.dateText}>
              {bookingDays > 1 ? `${bookingDays} days` : `${bookingDays} day`}
            </Text>
          </View>
        </View>
        <Text style={styles.priceText}>${totalPrice}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ListMybookingItem;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderColor: colors.borders,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemSection: {
    flex: 1,
    marginLeft: 20,
    height: '100%',
    paddingVertical: 5,
  },
  dummyImage: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  desc: {
    fontSize: 12,
    fontFamily: fonts.primary[300],
    color: colors.text.secondary,
    textTransform: 'capitalize',
  },
  buttonSection: {
    flexDirection: 'row',
  },
  initialContainer: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
    borderColor: colors.borders,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  initialText: {
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.primary,
  },
  dummyImageContainer: {
    width: 106,
    height: 106,
    borderRadius: 20,
    backgroundColor: colors.borders,
    justifyContent: 'center',
    alignItems: 'center',
  },
  locationDateSection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  priceText: {
    fontFamily: fonts.primary[500],
    fontSize: 16,
    color: colors.text.primary,
    marginTop: 10,
  },
  dateText: {
    fontSize: 12,
    fontFamily: fonts.primary[400],
    color: colors.text.primary,
  },
});
