import React from 'react';
import {View, StyleSheet, Text} from 'react-native';

const Footer = () => {
  return (
    <View style={styles.footer}>
      <Text>Footer Content Here</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  footer: {
    width: '100%',
    backgroundColor: 'white',
    shadowColor: '#C0C0C0',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 1,
    shadowRadius: 10,
    elevation: 5, // for Android
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Footer;
