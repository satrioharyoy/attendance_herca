import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome6';
import {Gap} from '../../atoms';

const Icon = ({icon}) => {
  switch (icon) {
    case 'removePhoto':
      return (
        <IconFontAwesome.Button
          name={'xmark'}
          color="white"
          style={styles.removePhoto}
        />
      );
    case 'male':
      return (
        <IconFontAwesome name="mars" color={colors.tertiary} solid size={24} />
      );
    case 'female':
      return (
        <IconFontAwesome
          name="venus"
          color={colors.errorMessage}
          solid
          size={24}
        />
      );
    default:
      return (
        <IconFontAwesome.Button name={'xmark'} style={styles.removePhoto} />
      );
  }
};

const Profile = ({name, desc, avatar, icon, isRemove, onPress}) => {
  const getInitialName = name?.split('')[0];
  return (
    <View style={styles.container}>
      {!isRemove && (
        <View style={styles.borderAvatar}>
          {avatar ? (
            <Image source={avatar} style={styles.avatar} />
          ) : (
            <Text style={styles.initialText}>{getInitialName}</Text>
          )}
        </View>
      )}
      {isRemove && (
        <TouchableOpacity style={styles.borderAvatar} onPress={onPress}>
          {avatar && <Image source={avatar} style={styles.avatar} />}
          {icon && <Icon icon={icon} />}
        </TouchableOpacity>
      )}
      <Gap height={10} />
      {/* {icon && <Icon icon={icon} />} */}
      {name && (
        <View>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.job}>{desc}</Text>
        </View>
      )}
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  avatar: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  borderAvatar: {
    width: 130,
    height: 130,
    borderRadius: 130 / 2,
    borderColor: colors.borders,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
    marginTop: 16,
    textAlign: 'center',
  },
  job: {
    fontFamily: fonts.primary[400],
    fontSize: 16,
    color: colors.text.secondary,
    marginTop: 2,
    textAlign: 'center',
  },
  initialText: {
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 28,
    fontWeight: 'bold',
    color: colors.primary,
  },
});
