import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {ImageNotFoundILL, WhislistWhiteIcon} from '../../../assets';
import {Gap, LocationName, RatingText} from '../../atoms';
import {fonts} from '../../../utils';

const HomestayRatingCard = ({name, price, rating, imageUrl}) => {
  return (
    <View style={styles.card}>
      <View style={styles.ratingContainer}>
        <View style={styles.ratingSection}>
          <RatingText size="small" rating={rating} type="light" />
        </View>
        <WhislistWhiteIcon />
      </View>
      <View style={{alignSelf: 'center'}}>
        <Image source={ImageNotFoundILL} width={10} />
      </View>
      <View>
        <Text style={styles.name}>{name}</Text>
        <Gap height={5} />
        <LocationName
          size={12}
          type={'light'}
          location={'Anyer'}
          province={'Serang'}
        />
        <Gap height={5} />
        <Text style={styles.description}>${price}/night</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: 'rgba(217, 217, 217, 1)',
    padding: 12,
    borderRadius: 10,
    alignSelf: 'flex-start',
    marginRight: 10,
    height: 220,
    justifyContent: 'space-between',
  },
  name: {
    fontSize: 14,
    fontFamily: fonts.primary[600],
    color: 'white',
  },
  description: {
    fontSize: 14,
    color: 'white',
    fontFamily: fonts.primary[500],
  },
  rating: {
    fontSize: 16,
    color: 'gold',
    fontWeight: 'bold',
  },
  ratingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  ratingSection: {
    borderRadius: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.12)',
    padding: 4,
  },
});

export default HomestayRatingCard;
