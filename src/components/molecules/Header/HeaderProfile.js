import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button} from '../../atoms';

const HeaderProfile = ({onPress, headerTitle, desc, photo}) => {
  return (
    <View style={styles.container}>
      <Button type="iconOnly" icon="iconBackLight" onPress={onPress} />
      <View style={styles.contentSection}>
        <Text style={styles.title}>{headerTitle}</Text>
        <Text style={styles.subTitle}>{desc}</Text>
      </View>
      <Image source={photo} style={styles.avatar} />
    </View>
  );
};

export default HeaderProfile;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    paddingVertical: 30,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  avatar: {
    width: 46,
    height: 46,
    borderRadius: 46 / 2,
  },
  contentSection: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    textAlign: 'center',
    color: colors.white,
  },
  subTitle: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    textAlign: 'center',
    color: colors.text.subTitle,
  },
});
