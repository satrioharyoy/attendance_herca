import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';
import HeaderProfile from './HeaderProfile';

const Header = ({headerTitle, onPress, icon, type, desc, photo, iconRight}) => {
  if (type === 'darkProfile') {
    return (
      <HeaderProfile
        onPress={onPress}
        headerTitle={headerTitle}
        desc={desc}
        photo={photo}
      />
    );
  }
  return (
    <View style={styles.container(type)}>
      {icon ? <Button type="iconOnly" onPress={onPress} icon={icon} /> : null}
      <Text style={styles.headerTitle(type)}>{headerTitle}</Text>
      <Gap width={24} />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: type => ({
    flexDirection: 'row',
    paddingVertical: 30,
    alignItems: 'center',
    backgroundColor: type === 'dark' ? colors.secondary : colors.white,
    borderBottomLeftRadius: type === 'dark' ? 20 : 0,
    borderBottomRightRadius: type === 'dark' ? 20 : 0,
    paddingHorizontal: 20,
  }),
  headerTitle: type => ({
    flex: 1,
    fontSize: 20,
    color: type === 'dark' ? colors.white : colors.text.primary,
    fontFamily: fonts.primary[600],
    textTransform: 'capitalize',
    marginLeft: 20,
  }),
});
