import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {MobileDarkIcon} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Loading = () => {
  return (
    <View style={styles.container}>
      <View style={styles.loadingWrapper}>
        <MobileDarkIcon />
        <Text style={styles.loadingText}>Loading Product Data</Text>
        <Text style={styles.loadingText1}>Please wait...</Text>
      </View>
    </View>
  );
};

export default Loading;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
  loadingWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingText: {
    fontSize: 20,
    fontFamily: fonts.primary[700],
    color: colors.text.primary,
    marginTop: 4,
  },
  loadingText1: {
    fontSize: 16,
    fontFamily: fonts.primary[400],
    marginTop: 4,
  },
});
