const mainColors = {
  green1: '#0BCAD4',
  green2: '#EDFCFD',
  dark1: '#171621',
  dark2: '#495A75',
  grey1: '#7D8285',
  grey2: '#E9E9E9',
  grey3: '#8092AF',
  grey4: '#EDEEF0',
  grey5: '#B1B7C2',
  blue1: '#0066CB',
  black1: 'black',
  black2: '#3A4144',
  red1: '#E06379',
  red2: '#D81A3C',
  yellow1: '#FA9F54',
};

export const colors = {
  primary: mainColors.yellow1,
  secondary: mainColors.red2,
  tertiary: mainColors.blue1,
  white: 'white',
  black: 'black',
  disable: mainColors.grey4,
  text: {
    primary: mainColors.black2,
    secondary: mainColors.grey1,
    menuInactive: mainColors.dark2,
    menuActive: mainColors.yellow1,
    subTitle: mainColors.grey3,
  },
  button: {
    primary: {
      background: mainColors.yellow1,
      text: 'white',
    },
    secondary: {
      background: 'white',
      text: mainColors.dark1,
    },
  },
  borders: mainColors.grey2,
  cardLight: mainColors.green2,
  loadingBackground: mainColors.black2,
  errorMessage: mainColors.red1,
  buttonDisable: {
    background: mainColors.grey4,
    text: mainColors.grey5,
  },
};
