export const fonts = {
  primary: {
    200: 'FiraSans-ExtraLight',
    300: 'FiraSans-Light',
    400: 'FiraSans-Regular',
    600: 'FiraSans-SemiBold',
    700: 'FiraSans-Bold',
    800: 'FiraSans-ExtraBold',
    900: 'FiraSans-Black',
    normal: 'FiraSans-Regular',
  },
};
