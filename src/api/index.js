import axios from 'axios';

export const getProductList = async value => {
  try {
    const res = await axios({
      url: 'https://dummyjson.com/products',
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
      timeout: 30000,
    });
    console.log('check res => ', res);
    const successResponse = {
      success: true,
      data: res.data.products,
      totalData: res.data.totalData,
    };
    return successResponse;
  } catch (error) {
    console.log('check error getEventList => ', error);
    const responseError = {
      error: true,
      ...error?.response?.data,
    };
    return responseError;
  }
};
