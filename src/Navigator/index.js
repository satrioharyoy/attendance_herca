import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {
  AddContact,
  AttendanceDetail,
  EditAttendance,
  Home,
  HomestayDetail,
  ListUsers,
  MyBookings,
  UserProfile,
} from '../screens';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigator} from '../components';
import Attedance from '../screens/Attendance';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
      <Tab.Screen
        name="MyBookings"
        component={MyBookings}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Promo"
        component={Attedance}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Profile"
        component={Attedance}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

const Navigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
      <Stack.Screen
        name="UserProfile"
        component={UserProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddContact"
        component={AddContact}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="AttendanceDetail"
        component={AttendanceDetail}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="EditAttendance"
        component={EditAttendance}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="AboutHomestay"
        component={HomestayDetail}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Navigator;
