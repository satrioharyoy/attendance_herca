import HomeInactivedIcon from './home_inactived_icon.svg';
import HomeActivedIcon from './home_actived_icon.svg';
import CalendarInactivedIcon from './calendar_inactived_icon.svg';
import CalendarActivedIcon from './calendar_actived_icon.svg';
import DiscountInactivedIcon from './discount_inactived_icon.svg';
import ProfileInactivedIcon from './profile_inactived_icon.svg';
import BackButtonIcon from './back_button_icon.svg';
import HamburgerIcon from './hamburger_icon.svg';
import PinLocationIcon from './pin_location_icon.svg';
import ClockIcon from './clock_icon.svg';
import DateIcon from './date_icon.svg';
import WhislistIcon from './whislist_icon.svg';
import StarIcon from './star_icon.svg';
import WifiIcon from './wifi_icon.svg';
import FitnessIcon from './fitness_icon.svg';
import BreakfastIcon from './breakfast_icon.svg';
import BeachIcon from './beach_icon.svg';
import SearchIcon from './search_icon.svg';
import FilterIcon from './filter_icon.svg';
import BellIcon from './bell_icon.svg';
import WhislistWhiteIcon from './whislist_white_icon.svg';
import PinLocationWhiteIcon from './pin_location_white.svg';
import MobileIcon from './mobile_icon.svg';
import MobileDarkIcon from './icon_mobile_dark.svg';

export {
  HomeInactivedIcon,
  HomeActivedIcon,
  CalendarInactivedIcon,
  CalendarActivedIcon,
  DiscountInactivedIcon,
  ProfileInactivedIcon,
  BackButtonIcon,
  HamburgerIcon,
  PinLocationIcon,
  ClockIcon,
  DateIcon,
  WhislistIcon,
  StarIcon,
  WifiIcon,
  FitnessIcon,
  BreakfastIcon,
  BeachIcon,
  SearchIcon,
  FilterIcon,
  BellIcon,
  WhislistWhiteIcon,
  PinLocationWhiteIcon,
  MobileDarkIcon,
  MobileIcon,
};
