import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {LogBox} from 'react-native';
import FlashMessage from 'react-native-flash-message';
import Navigator from './Navigator';
import {AttendanceProvider} from './context/userContext';

const MainApp = () => {
  LogBox.ignoreLogs(['Setting a timer']);

  return (
    <AttendanceProvider>
      <NavigationContainer>
        <Navigator />
      </NavigationContainer>
      <FlashMessage position="top" />
    </AttendanceProvider>
  );
};
const App = () => {
  return <MainApp />;
};
export default App;
